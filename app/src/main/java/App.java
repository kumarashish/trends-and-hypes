import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;
import io.realm.Realm;

public class App extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

      //
    }

    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }
}
