package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;
import io.realm.annotations.RealmClass;

@RealmClass
public class Pagination extends RealmObject {

    @SerializedName("current_page")
    private int currentPage;

    @SerializedName("per_page")
    private int perPage;

    @SerializedName("next_page")
    private String nextPage;

    public int getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(int currentPage) {
        this.currentPage = currentPage;
    }

    public int getPerPage() {
        return perPage;
    }

    public void setPerPage(int perPage) {
        this.perPage = perPage;
    }

    public String getNextPage() {
        return nextPage;
    }

    public void setNextPage(String nextPage) {
        this.nextPage = nextPage;
    }
}
