package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class Staff{

	@SerializedName("owner")
	private String owner;

	@SerializedName("host")
	private String host;

	@SerializedName("chef")
	private String chef;

	@SerializedName("sommelier")
	private String sommelier;

	public void setOwner(String owner){
		this.owner = owner;
	}

	public String getOwner(){
		return owner;
	}

	public void setHost(String host){
		this.host = host;
	}

	public String getHost(){
		return host;
	}

	public void setChef(String chef){
		this.chef = chef;
	}

	public String getChef(){
		return chef;
	}

	public void setSommelier(String sommelier){
		this.sommelier = sommelier;
	}

	public String getSommelier(){
		return sommelier;
	}

	@Override
 	public String toString(){
		return 
			"Staff{" + 
			"owner = '" + owner + '\'' + 
			",host = '" + host + '\'' + 
			",chef = '" + chef + '\'' + 
			",sommelier = '" + sommelier + '\'' + 
			"}";
		}
}
