package nu.eet.restaurantapp.data.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Response{

	@SerializedName("rating")
	private int rating;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("description")
	private String description;

	@SerializedName("reservations_url")
	private String reservationsUrl;

	@SerializedName("capacity")
	private Capacity capacity;

	@SerializedName("twitter")
	private String twitter;

	@SerializedName("updated_at")
	private String updatedAt;

	@SerializedName("holidays")
	private List<HolidaysItem> holidays;

	@SerializedName("id")
	private int id;

	@SerializedName("reachability")
	private Reachability reachability;

	@SerializedName("menus")
	private List<MenusItem> menus;

	@SerializedName("prices")
	private Prices prices;

	@SerializedName("plan")
	private String plan;

	@SerializedName("vote_by_current_user")
	private Object voteByCurrentUser;

	@SerializedName("facebook_url")
	private String facebookUrl;

	@SerializedName("maintainers")
	private List<MaintainersItem> maintainers;

	@SerializedName("images")
	private Images images;

	@SerializedName("address")
	private Address address;

	@SerializedName("counters")
	private Counters counters;

	@SerializedName("currency_symbol")
	private String currencySymbol;

	@SerializedName("mobile")
	private Object mobile;

	@SerializedName("resources")
	private Resources resources;

	@SerializedName("telephone")
	private String telephone;

	@SerializedName("staff")
	private Staff staff;

	@SerializedName("url")
	private String url;

	@SerializedName("tags")
	private List<String> tags;

	@SerializedName("embeddables")
	private Embeddables embeddables;

	@SerializedName("highlights")
	private Highlights highlights;

	@SerializedName("website_url")
	private String websiteUrl;

	@SerializedName("instagram_url")
	private String instagramUrl;

	@SerializedName("rating_by_current_user")
	private Object ratingByCurrentUser;

	@SerializedName("name")
	private String name;

	@SerializedName("opening_hours")
	private List<OpeningHoursItem> openingHours;

	@SerializedName("tagline")
	private String tagline;

	@SerializedName("category")
	private String category;

	@SerializedName("geolocation")
	private Geolocation geolocation;

	@SerializedName("status")
	private String status;

	public void setRating(int rating){
		this.rating = rating;
	}

	public int getRating(){
		return rating;
	}

	public void setCreatedAt(String createdAt){
		this.createdAt = createdAt;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setReservationsUrl(String reservationsUrl){
		this.reservationsUrl = reservationsUrl;
	}

	public String getReservationsUrl(){
		return reservationsUrl;
	}

	public void setCapacity(Capacity capacity){
		this.capacity = capacity;
	}

	public Capacity getCapacity(){
		return capacity;
	}

	public void setTwitter(String twitter){
		this.twitter = twitter;
	}

	public String getTwitter(){
		return twitter;
	}

	public void setUpdatedAt(String updatedAt){
		this.updatedAt = updatedAt;
	}

	public String getUpdatedAt(){
		return updatedAt;
	}

	public void setHolidays(List<HolidaysItem> holidays){
		this.holidays = holidays;
	}

	public List<HolidaysItem> getHolidays(){
		return holidays;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setReachability(Reachability reachability){
		this.reachability = reachability;
	}

	public Reachability getReachability(){
		return reachability;
	}

	public void setMenus(List<MenusItem> menus){
		this.menus = menus;
	}

	public List<MenusItem> getMenus(){
		return menus;
	}

	public void setPrices(Prices prices){
		this.prices = prices;
	}

	public Prices getPrices(){
		return prices;
	}

	public void setPlan(String plan){
		this.plan = plan;
	}

	public String getPlan(){
		return plan;
	}

	public void setVoteByCurrentUser(Object voteByCurrentUser){
		this.voteByCurrentUser = voteByCurrentUser;
	}

	public Object getVoteByCurrentUser(){
		return voteByCurrentUser;
	}

	public void setFacebookUrl(String facebookUrl){
		this.facebookUrl = facebookUrl;
	}

	public String getFacebookUrl(){
		return facebookUrl;
	}

	public void setMaintainers(List<MaintainersItem> maintainers){
		this.maintainers = maintainers;
	}

	public List<MaintainersItem> getMaintainers(){
		return maintainers;
	}

	public void setImages(Images images){
		this.images = images;
	}

	public Images getImages(){
		return images;
	}

	public void setAddress(Address address){
		this.address = address;
	}

	public Address getAddress(){
		return address;
	}

	public void setCounters(Counters counters){
		this.counters = counters;
	}

	public Counters getCounters(){
		return counters;
	}

	public void setCurrencySymbol(String currencySymbol){
		this.currencySymbol = currencySymbol;
	}

	public String getCurrencySymbol(){
		return currencySymbol;
	}

	public void setMobile(Object mobile){
		this.mobile = mobile;
	}

	public Object getMobile(){
		return mobile;
	}

	public void setResources(Resources resources){
		this.resources = resources;
	}

	public Resources getResources(){
		return resources;
	}

	public void setTelephone(String telephone){
		this.telephone = telephone;
	}

	public String getTelephone(){
		return telephone;
	}

	public void setStaff(Staff staff){
		this.staff = staff;
	}

	public Staff getStaff(){
		return staff;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	public void setTags(List<String> tags){
		this.tags = tags;
	}

	public List<String> getTags(){
		return tags;
	}

	public void setEmbeddables(Embeddables embeddables){
		this.embeddables = embeddables;
	}

	public Embeddables getEmbeddables(){
		return embeddables;
	}

	public void setHighlights(Highlights highlights){
		this.highlights = highlights;
	}

	public Highlights getHighlights(){
		return highlights;
	}

	public void setWebsiteUrl(String websiteUrl){
		this.websiteUrl = websiteUrl;
	}

	public String getWebsiteUrl(){
		return websiteUrl;
	}

	public void setInstagramUrl(String instagramUrl){
		this.instagramUrl = instagramUrl;
	}

	public String getInstagramUrl(){
		return instagramUrl;
	}

	public void setRatingByCurrentUser(Object ratingByCurrentUser){
		this.ratingByCurrentUser = ratingByCurrentUser;
	}

	public Object getRatingByCurrentUser(){
		return ratingByCurrentUser;
	}
	
	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setOpeningHours(List<OpeningHoursItem> openingHours){
		this.openingHours = openingHours;
	}

	public List<OpeningHoursItem> getOpeningHours(){
		return openingHours;
	}

	public void setTagline(String tagline){
		this.tagline = tagline;
	}

	public String getTagline(){
		return tagline;
	}

	public void setCategory(String category){
		this.category = category;
	}

	public String getCategory(){
		return category;
	}

	public void setGeolocation(Geolocation geolocation){
		this.geolocation = geolocation;
	}

	public Geolocation getGeolocation(){
		return geolocation;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"Response{" + 
			"rating = '" + rating + '\'' + 
			",created_at = '" + createdAt + '\'' + 
			",description = '" + description + '\'' + 
			",reservations_url = '" + reservationsUrl + '\'' + 
			",capacity = '" + capacity + '\'' + 
			",twitter = '" + twitter + '\'' + 
			",updated_at = '" + updatedAt + '\'' + 
			",holidays = '" + holidays + '\'' + 
			",id = '" + id + '\'' + 
			",reachability = '" + reachability + '\'' + 
			",menus = '" + menus + '\'' + 
			",prices = '" + prices + '\'' + 
			",plan = '" + plan + '\'' + 
			",vote_by_current_user = '" + voteByCurrentUser + '\'' + 
			",facebook_url = '" + facebookUrl + '\'' + 
			",maintainers = '" + maintainers + '\'' + 
			",images = '" + images + '\'' + 
			",address = '" + address + '\'' + 
			",counters = '" + counters + '\'' + 
			",currency_symbol = '" + currencySymbol + '\'' + 
			",mobile = '" + mobile + '\'' + 
			",resources = '" + resources + '\'' + 
			",telephone = '" + telephone + '\'' + 
			",staff = '" + staff + '\'' + 
			",url = '" + url + '\'' + 
			",tags = '" + tags + '\'' + 
			",embeddables = '" + embeddables + '\'' + 
			",highlights = '" + highlights + '\'' + 
			",website_url = '" + websiteUrl + '\'' + 
			",instagram_url = '" + instagramUrl + '\'' + 
			",rating_by_current_user = '" + ratingByCurrentUser + '\'' +
			",name = '" + name + '\'' + 
			",opening_hours = '" + openingHours + '\'' + 
			",tagline = '" + tagline + '\'' + 
			",category = '" + category + '\'' + 
			",geolocation = '" + geolocation + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
