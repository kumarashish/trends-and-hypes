package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class OpeningHoursItem{

	@SerializedName("closed")
	private boolean closed;

	@SerializedName("day")
	private int day;

	@SerializedName("lunch_from")
	private String lunchFrom;

	@SerializedName("lunch_till")
	private String lunchTill;

	public void setClosed(boolean closed){
		this.closed = closed;
	}

	public boolean isClosed(){
		return closed;
	}

	public void setDay(int day){
		this.day = day;
	}

	public int getDay(){
		return day;
	}

	public void setLunchFrom(String lunchFrom){
		this.lunchFrom = lunchFrom;
	}

	public String getLunchFrom(){
		return lunchFrom;
	}

	public void setLunchTill(String lunchTill){
		this.lunchTill = lunchTill;
	}

	public String getLunchTill(){
		return lunchTill;
	}

	@Override
 	public String toString(){
		return 
			"OpeningHoursItem{" + 
			"closed = '" + closed + '\'' + 
			",day = '" + day + '\'' + 
			",lunch_from = '" + lunchFrom + '\'' + 
			",lunch_till = '" + lunchTill + '\'' + 
			"}";
		}
}
