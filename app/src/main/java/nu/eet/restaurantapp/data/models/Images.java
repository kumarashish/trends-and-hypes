package nu.eet.restaurantapp.data.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class Images{

	@SerializedName("original")
	private List<String> original;

	@SerializedName("cropped")
	private List<String> cropped;

	public void setOriginal(List<String> original){
		this.original = original;
	}

	public List<String> getOriginal(){
		return original;
	}

	public void setCropped(List<String> cropped){
		this.cropped = cropped;
	}

	public List<String> getCropped(){
		return cropped;
	}

	@Override
 	public String toString(){
		return 
			"Images{" + 
			"original = '" + original + '\'' + 
			",cropped = '" + cropped + '\'' + 
			"}";
		}
}
