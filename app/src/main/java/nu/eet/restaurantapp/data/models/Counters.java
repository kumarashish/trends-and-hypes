package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class Counters{

	@SerializedName("images")
	private int images;

	@SerializedName("reviews")
	private int reviews;

	@SerializedName("ratings")
	private Ratings ratings;

	@SerializedName("votes")
	private Votes votes;

	@SerializedName("menus")
	private int menus;

	@SerializedName("fans")
	private int fans;

	public void setImages(int images){
		this.images = images;
	}

	public int getImages(){
		return images;
	}

	public void setReviews(int reviews){
		this.reviews = reviews;
	}

	public int getReviews(){
		return reviews;
	}

	public void setRatings(Ratings ratings){
		this.ratings = ratings;
	}

	public Ratings getRatings(){
		return ratings;
	}

	public void setVotes(Votes votes){
		this.votes = votes;
	}

	public Votes getVotes(){
		return votes;
	}

	public void setMenus(int menus){
		this.menus = menus;
	}

	public int getMenus(){
		return menus;
	}

	public void setFans(int fans){
		this.fans = fans;
	}

	public int getFans(){
		return fans;
	}

	@Override
 	public String toString(){
		return 
			"Counters{" + 
			"images = '" + images + '\'' + 
			",reviews = '" + reviews + '\'' + 
			",ratings = '" + ratings + '\'' + 
			",votes = '" + votes + '\'' + 
			",menus = '" + menus + '\'' + 
			",fans = '" + fans + '\'' + 
			"}";
		}
}
