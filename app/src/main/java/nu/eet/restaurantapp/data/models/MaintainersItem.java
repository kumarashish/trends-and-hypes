package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class MaintainersItem{

	@SerializedName("profile_id")
	private int profileId;

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("url")
	private String url;

	public void setProfileId(int profileId){
		this.profileId = profileId;
	}

	public int getProfileId(){
		return profileId;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	@Override
 	public String toString(){
		return 
			"MaintainersItem{" + 
			"profile_id = '" + profileId + '\'' + 
			",name = '" + name + '\'' + 
			",id = '" + id + '\'' + 
			",url = '" + url + '\'' + 
			"}";
		}
}
