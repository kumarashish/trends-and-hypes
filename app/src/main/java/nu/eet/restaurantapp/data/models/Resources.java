package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class Resources{

	@SerializedName("country")
	private String country;

	@SerializedName("reviews")
	private String reviews;

	@SerializedName("reservations")
	private String reservations;

	@SerializedName("city")
	private String city;

	@SerializedName("self")
	private String self;

	@SerializedName("reservation_restaurant")
	private String reservationRestaurant;

	@SerializedName("region")
	private String region;

	@SerializedName("new_reservation")
	private String newReservation;

	@SerializedName("vote")
	private String vote;

	public void setCountry(String country){
		this.country = country;
	}

	public String getCountry(){
		return country;
	}

	public void setReviews(String reviews){
		this.reviews = reviews;
	}

	public String getReviews(){
		return reviews;
	}

	public void setReservations(String reservations){
		this.reservations = reservations;
	}

	public String getReservations(){
		return reservations;
	}

	public void setCity(String city){
		this.city = city;
	}

	public String getCity(){
		return city;
	}

	public void setSelf(String self){
		this.self = self;
	}

	public String getSelf(){
		return self;
	}

	public void setReservationRestaurant(String reservationRestaurant){
		this.reservationRestaurant = reservationRestaurant;
	}

	public String getReservationRestaurant(){
		return reservationRestaurant;
	}

	public void setRegion(String region){
		this.region = region;
	}

	public String getRegion(){
		return region;
	}

	public void setNewReservation(String newReservation){
		this.newReservation = newReservation;
	}

	public String getNewReservation(){
		return newReservation;
	}

	public void setVote(String vote){
		this.vote = vote;
	}

	public String getVote(){
		return vote;
	}

	@Override
 	public String toString(){
		return 
			"Resources{" + 
			"country = '" + country + '\'' + 
			",reviews = '" + reviews + '\'' + 
			",reservations = '" + reservations + '\'' + 
			",city = '" + city + '\'' + 
			",self = '" + self + '\'' + 
			",reservation_restaurant = '" + reservationRestaurant + '\'' + 
			",region = '" + region + '\'' + 
			",new_reservation = '" + newReservation + '\'' + 
			",vote = '" + vote + '\'' + 
			"}";
		}
}
