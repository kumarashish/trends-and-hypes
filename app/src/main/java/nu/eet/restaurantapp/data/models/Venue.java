package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;
import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;
import io.realm.annotations.RealmClass;

@RealmClass
public class Venue extends RealmObject {
    
    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String name;

    @SerializedName("rating")
    private String rating;

    @SerializedName("category")
    private String category;
    
    public int getId() { return id; }

    public void setId(int id) { this.id = id; }

    public String getName() { return name; }

    public String getRating() { return rating; }

    public void setRating(String rating) { this.rating = rating; }

    public String getCategory() { return category; }

    public void setCategory(String category) { this.category = category; }

    public void setName(String name) { this.name = name; }
}
