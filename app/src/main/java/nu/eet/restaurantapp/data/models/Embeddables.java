package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class Embeddables{

	@SerializedName("reservations")
	private String reservations;

	public void setReservations(String reservations){
		this.reservations = reservations;
	}

	public String getReservations(){
		return reservations;
	}

	@Override
 	public String toString(){
		return 
			"Embeddables{" + 
			"reservations = '" + reservations + '\'' + 
			"}";
		}
}
