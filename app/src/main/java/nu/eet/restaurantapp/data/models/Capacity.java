package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class Capacity{

	@SerializedName("seats_per_group")
	private int seatsPerGroup;

	@SerializedName("seats")
	private int seats;

	public void setSeatsPerGroup(int seatsPerGroup){
		this.seatsPerGroup = seatsPerGroup;
	}

	public int getSeatsPerGroup(){
		return seatsPerGroup;
	}

	public void setSeats(int seats){
		this.seats = seats;
	}

	public int getSeats(){
		return seats;
	}

	@Override
 	public String toString(){
		return 
			"Capacity{" + 
			"seats_per_group = '" + seatsPerGroup + '\'' + 
			",seats = '" + seats + '\'' + 
			"}";
		}
}
