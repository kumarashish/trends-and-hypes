package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class MenusItem{

	@SerializedName("content_type")
	private String contentType;

	@SerializedName("attachment")
	private Attachment attachment;

	@SerializedName("name")
	private String name;

	@SerializedName("resources")
	private Resources resources;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private String type;

	@SerializedName("url")
	private String url;

	public void setContentType(String contentType){
		this.contentType = contentType;
	}

	public String getContentType(){
		return contentType;
	}

	public void setAttachment(Attachment attachment){
		this.attachment = attachment;
	}

	public Attachment getAttachment(){
		return attachment;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setResources(Resources resources){
		this.resources = resources;
	}

	public Resources getResources(){
		return resources;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	@Override
 	public String toString(){
		return 
			"MenusItem{" + 
			"content_type = '" + contentType + '\'' + 
			",attachment = '" + attachment + '\'' + 
			",name = '" + name + '\'' + 
			",resources = '" + resources + '\'' + 
			",id = '" + id + '\'' + 
			",type = '" + type + '\'' + 
			",url = '" + url + '\'' + 
			"}";
		}
}
