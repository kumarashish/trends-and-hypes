package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class Ratings{

	@SerializedName("negative")
	private int negative;

	@SerializedName("positive")
	private int positive;

	public void setNegative(int negative){
		this.negative = negative;
	}

	public int getNegative(){
		return negative;
	}

	public void setPositive(int positive){
		this.positive = positive;
	}

	public int getPositive(){
		return positive;
	}

	@Override
 	public String toString(){
		return 
			"Ratings{" + 
			"negative = '" + negative + '\'' + 
			",positive = '" + positive + '\'' + 
			"}";
		}
}
