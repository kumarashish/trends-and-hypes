package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class Prices{

	@SerializedName("courses_6")
	private Object courses6;

	@SerializedName("courses_5")
	private Object courses5;

	@SerializedName("high_tea")
	private double highTea;

	@SerializedName("courses_4")
	private Object courses4;

	@SerializedName("courses_3")
	private Object courses3;

	@SerializedName("courses_3_a_la_carte")
	private double courses3ALaCarte;

	@SerializedName("buffet")
	private double buffet;

	@SerializedName("price_range")
	private String priceRange;

	public void setCourses6(Object courses6){
		this.courses6 = courses6;
	}

	public Object getCourses6(){
		return courses6;
	}

	public void setCourses5(Object courses5){
		this.courses5 = courses5;
	}

	public Object getCourses5(){
		return courses5;
	}

	public void setHighTea(double highTea){
		this.highTea = highTea;
	}

	public double getHighTea(){
		return highTea;
	}

	public void setCourses4(Object courses4){
		this.courses4 = courses4;
	}

	public Object getCourses4(){
		return courses4;
	}

	public void setCourses3(Object courses3){
		this.courses3 = courses3;
	}

	public Object getCourses3(){
		return courses3;
	}

	public void setCourses3ALaCarte(double courses3ALaCarte){
		this.courses3ALaCarte = courses3ALaCarte;
	}

	public double getCourses3ALaCarte(){
		return courses3ALaCarte;
	}

	public void setBuffet(double buffet){
		this.buffet = buffet;
	}

	public double getBuffet(){
		return buffet;
	}

	public void setPriceRange(String priceRange){
		this.priceRange = priceRange;
	}

	public String getPriceRange(){
		return priceRange;
	}

	@Override
 	public String toString(){
		return 
			"Prices{" + 
			"courses_6 = '" + courses6 + '\'' + 
			",courses_5 = '" + courses5 + '\'' + 
			",high_tea = '" + highTea + '\'' + 
			",courses_4 = '" + courses4 + '\'' + 
			",courses_3 = '" + courses3 + '\'' + 
			",courses_3_a_la_carte = '" + courses3ALaCarte + '\'' + 
			",buffet = '" + buffet + '\'' + 
			",price_range = '" + priceRange + '\'' + 
			"}";
		}
}
