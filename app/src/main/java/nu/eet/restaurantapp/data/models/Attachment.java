package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class Attachment{

	@SerializedName("content_type")
	private String contentType;

	@SerializedName("url")
	private String url;

	public void setContentType(String contentType){
		this.contentType = contentType;
	}

	public String getContentType(){
		return contentType;
	}

	public void setUrl(String url){
		this.url = url;
	}

	public String getUrl(){
		return url;
	}

	@Override
 	public String toString(){
		return 
			"Attachment{" + 
			"content_type = '" + contentType + '\'' + 
			",url = '" + url + '\'' + 
			"}";
		}
}
