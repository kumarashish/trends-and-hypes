package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class Reachability{

	@SerializedName("parking")
	private String parking;

	@SerializedName("public_transportation")
	private String publicTransportation;

	public void setParking(String parking){
		this.parking = parking;
	}

	public String getParking(){
		return parking;
	}

	public void setPublicTransportation(String publicTransportation){
		this.publicTransportation = publicTransportation;
	}

	public String getPublicTransportation(){
		return publicTransportation;
	}

	@Override
 	public String toString(){
		return 
			"Reachability{" + 
			"parking = '" + parking + '\'' + 
			",public_transportation = '" + publicTransportation + '\'' + 
			"}";
		}
}
