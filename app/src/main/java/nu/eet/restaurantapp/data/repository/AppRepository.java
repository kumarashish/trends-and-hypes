package nu.eet.restaurantapp.data.repository;

import io.realm.Realm;
import io.realm.RealmResults;
import nu.eet.restaurantapp.data.models.Venue;
import java.util.*;
public class AppRepository implements Repository<Venue>  {
    
    @Override
    public void add(final Venue item) {
        // Get a Realm instance for this thread
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(item);
            }
        });
    }
    
    @Override
    public void deleteAll() {
        Realm.getDefaultInstance().delete(Venue.class);
    }
    
    public void add(final List<Venue> items,Realm.Transaction.OnSuccess success) {
        Realm realm = Realm.getDefaultInstance();
        
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                realm.insert(items);
            }
        },success);
    }
   
    public void deleteItem(final Venue item) {
        Realm realm = Realm.getDefaultInstance();
        realm.executeTransactionAsync(new Realm.Transaction() {
            @Override
            public void execute(Realm realm) {
                RealmResults<Venue> rows = realm.where(Venue.class).equalTo("id",item.getId()).findAll();
                rows.deleteAllFromRealm();
            }
        });
    }
}
