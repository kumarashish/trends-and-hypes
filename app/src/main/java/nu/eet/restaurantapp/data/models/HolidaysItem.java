package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;

public class HolidaysItem{

	@SerializedName("starts_at")
	private String startsAt;

	@SerializedName("label")
	private Object label;

	@SerializedName("ends_at")
	private Object endsAt;

	@SerializedName("status")
	private String status;

	public void setStartsAt(String startsAt){
		this.startsAt = startsAt;
	}

	public String getStartsAt(){
		return startsAt;
	}

	public void setLabel(Object label){
		this.label = label;
	}

	public Object getLabel(){
		return label;
	}

	public void setEndsAt(Object endsAt){
		this.endsAt = endsAt;
	}

	public Object getEndsAt(){
		return endsAt;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	@Override
 	public String toString(){
		return 
			"HolidaysItem{" + 
			"starts_at = '" + startsAt + '\'' + 
			",label = '" + label + '\'' + 
			",ends_at = '" + endsAt + '\'' + 
			",status = '" + status + '\'' + 
			"}";
		}
}
