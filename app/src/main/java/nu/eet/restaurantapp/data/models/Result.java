package nu.eet.restaurantapp.data.models;

import com.google.gson.annotations.SerializedName;
import io.realm.RealmList;
import io.realm.RealmObject;


public class Result extends RealmObject {

    @SerializedName("results")
    private RealmList<Venue> result;
    
    @SerializedName("pagination")
    private Pagination pagination;

    public RealmList<Venue> getResult() {
        return result;
    }

    public void setResult(RealmList<Venue> result) {
        this.result = result;
    }

    public Pagination getPagination() {
        return pagination;
    }

    public void setPagination(Pagination pagination) {
        this.pagination = pagination;
    }
}
