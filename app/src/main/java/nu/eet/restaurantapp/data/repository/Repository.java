package nu.eet.restaurantapp.data.repository;

import java.util.List;

public interface Repository<T>  {
    void add(T item);
    void deleteAll();
}
