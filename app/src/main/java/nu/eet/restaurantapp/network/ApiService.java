package nu.eet.restaurantapp.network;

import nu.eet.restaurantapp.data.models.Result;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Url;

public interface ApiService {
    
    @GET("/venues")
    Call<Result> getAllVenues();

    @GET
    Call<Result> getRemaining(@Url String url);
}
