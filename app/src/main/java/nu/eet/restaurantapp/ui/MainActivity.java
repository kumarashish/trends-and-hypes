package nu.eet.restaurantapp.ui;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import io.realm.Realm;
import nu.eet.restaurantapp.R;
import nu.eet.restaurantapp.data.models.Result;
import nu.eet.restaurantapp.data.models.Venue;
import nu.eet.restaurantapp.data.repository.AppRepository;
import nu.eet.restaurantapp.network.ApiClient;
import nu.eet.restaurantapp.network.ApiService;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    
    ApiClient apiClient = new ApiClient();
    List<Venue> allVenues = new ArrayList<>();
    AppRepository repository = new AppRepository();
    boolean hasNextPage = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Realm.init(this);
        setContentView(R.layout.activity_main2);
        //GET all venues API call
        apiClient.getClient().create(ApiService.class).getAllVenues().enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                //Check response if 200 and success
                if (response.isSuccessful() && response.code() == 200) {
                    //Get next page due to pagination of data.
                    String urlNextPage = response.body().getPagination().getNextPage();
                    makeCall(urlNextPage);
                }
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) { }
        });
    }
    
    //Recursive function to fetch all venues until there is next page
    private void makeCall(final String nextUrl) {
        apiClient.getClient().create(ApiService.class).getRemaining(nextUrl).enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                //Check status code
                if (response.isSuccessful() && response.code() == 200) {
                    String urlNextPage = response.body().getPagination().getNextPage();
                    allVenues.addAll(response.body().getResult());
                    if (allVenues.size() == 200) {
                        urlNextPage = null;
                    }
                    if (urlNextPage != null) {
                        hasNextPage = true;
                        makeCall(urlNextPage);
                    } else {
                        Log.e("venue count", allVenues.size() + "");
                        hasNextPage = false;
                        //Save to REALM
                        repository.add(allVenues, new Realm.Transaction.OnSuccess() {
                            @Override
                            public void onSuccess() {
                                MainActivity.this.finish();
                                startActivity(new Intent(MainActivity.this, ListActivity.class));
                            }
                        });
                    }
                }
            }
            @Override
            public void onFailure(Call<Result> call, Throwable t) {

            }
        });
    }
}
