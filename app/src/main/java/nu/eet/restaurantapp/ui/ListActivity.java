
package nu.eet.restaurantapp.ui;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import io.realm.Realm;
import nu.eet.restaurantapp.R;
import nu.eet.restaurantapp.data.models.Venue;
import nu.eet.restaurantapp.data.repository.AppRepository;

import java.util.List;

public class ListActivity extends AppCompatActivity implements VenueAdapter.ItemClickListener {
    VenueAdapter adapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);
        
        
        List<Venue> data = Realm.getDefaultInstance().where(Venue.class).findAll();

        RecyclerView recyclerView = findViewById(R.id.rvAnimals);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new VenueAdapter(this, data);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onItemClick(View view, int position) {
            startActivity(new Intent(this,DetailActivity.class));
    }
}
